﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusyDotBat
{
    public struct Combination
    {
        public ConsoleModifiers ConsoleModifiers;
        public char KeyChar;

        public Combination(ConsoleModifiers mod, char c)
        {
            ConsoleModifiers = mod; KeyChar = Char.ToLower(c);
        }
    }


    public class Combinations : IDictionary<Combination, Action>
    {

        private Dictionary<Combination,Action> _dictionary = new Dictionary<Combination, Action>();

        public Action this[Combination key]
        {
            get
            {
                return ((IDictionary<Combination, Action>)_dictionary)[key];
            }

            set
            {
                ((IDictionary<Combination, Action>)_dictionary)[key] = value;
            }
        }

        public Action this[ConsoleModifiers mod, char c]
        {
            get
            {
                return ((IDictionary<Combination, Action>)_dictionary)[ new Combination(mod, c)];
            }
        }

        public int Count
        {
            get
            {
                return ((IDictionary<Combination, Action>)_dictionary).Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return ((IDictionary<Combination, Action>)_dictionary).IsReadOnly;
            }
        }

        public ICollection<Combination> Keys
        {
            get
            {
                return ((IDictionary<Combination, Action>)_dictionary).Keys;
            }
        }

        public ICollection<Action> Values
        {
            get
            {
                return ((IDictionary<Combination, Action>)_dictionary).Values;
            }
        }

        public void Add(KeyValuePair<Combination, Action> item)
        {
            ((IDictionary<Combination, Action>)_dictionary).Add(item);
        }

        public void Add(Combination key, Action value)
        {
            ((IDictionary<Combination, Action>)_dictionary).Add(key, value);
        }

        public void Add(ConsoleModifiers mod, char key, Action action)
        {
            ((IDictionary<Combination, Action>)_dictionary).Add(new Combination(mod, key), action);
        }

        public void Clear()
        {
            ((IDictionary<Combination, Action>)_dictionary).Clear();
        }

        public bool Contains(KeyValuePair<Combination, Action> item)
        {
            return ((IDictionary<Combination, Action>)_dictionary).Contains(item);
        }

        public bool ContainsKey(Combination key)
        {
            return ((IDictionary<Combination, Action>)_dictionary).ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<Combination, Action>[] array, int arrayIndex)
        {
            ((IDictionary<Combination, Action>)_dictionary).CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<Combination, Action>> GetEnumerator()
        {
            return ((IDictionary<Combination, Action>)_dictionary).GetEnumerator();
        }

        public bool Remove(KeyValuePair<Combination, Action> item)
        {
            return ((IDictionary<Combination, Action>)_dictionary).Remove(item);
        }

        public bool Remove(Combination key)
        {
            return ((IDictionary<Combination, Action>)_dictionary).Remove(key);
        }

        public bool TryGetValue(Combination key, out Action value)
        {
            return ((IDictionary<Combination, Action>)_dictionary).TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IDictionary<Combination, Action>)_dictionary).GetEnumerator();
        }
    }
}
