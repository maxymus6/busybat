﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace BusyDotBat
{
    public static class SMConstants
    {

        public static readonly string cconBitPrivJack = "000000000000001";
        public static readonly string cconBitPrivNehal = "000000000000010";
        public static readonly string cconBitPrivJane = "000000000000100";
        public static readonly string cconBitPrivLewis= "000000000001000";
        public static readonly string cconBitPrivRicky= "000000000010000";
        public static readonly string cconBitPrivDicy = "000000000100000";


        [ChannelBitPriv]
        public static readonly string cconCAChannelAtribue1 = "000000000100000";
        [ChannelBitPriv]
        public static readonly string cconCAChannelAtribue2 = "000000000100000";
        [ChannelBitPriv]
        public static readonly string cconCAChannelAtribue3 = "000000000100000";
        [ChannelBitPriv]
        public static readonly string cconCAChannelAtribue4 = "000000000100000";
        [ChannelBitPriv]
        public static readonly string cconCAChannelAtribue5 = "000000000100000";

        public static readonly string cconInProgress = "IP";
        public static readonly string cconApproved = "AP";
        public static readonly string cconSubmitted = "SM";
        public static readonly string cconDead = "DD";
        public static readonly string cconCAlive = "AL";
    }

    public class ChannelBitPriv : Attribute
    {
        
    }



    /*
    public Dictionary<string,bool> GetAttributes()
    {
        var x = new Dictionary<string, bool>();

        var types = typeof(SMConstants).GetProperties();




    }
    */

}
