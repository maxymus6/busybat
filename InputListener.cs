﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BusyDotBat
{
    public class InputListener 
    {
        public string Input = "";
        public Thread ListenerThread;
        public List<Thread> Workers = new List<Thread>();

        public InputListener()
        {
            ListenerThread = new Thread(new ThreadStart(Run))
            {
                IsBackground = false,
                Priority = ThreadPriority.Highest
            };
        }

        public void StartListening()
        {
            ListenerThread.Start();
        }

        public IDictionary<Combination,Action> Combinations = new Combinations
        {
            { ConsoleModifiers.Alt, 'O', Actions.WriteSomeStuff },
            { ConsoleModifiers.Alt, 'N',  Actions.WriteSomeNumbers },
            { ConsoleModifiers.Alt, 'S',  Actions.WriteSomeStrings },
            { ConsoleModifiers.Alt, 'D',  Actions.ListDirectories }

        };

        public void Run()
        {
            while(true)
            {
                var pressage = Console.ReadKey(true);

                if (Combinations.Any( x=> pressage.Modifiers == x.Key.ConsoleModifiers && pressage.KeyChar == x.Key.KeyChar ))
                {
                    PerformAction(pressage);
                }
                else
                {
                    Input += pressage.KeyChar;
                    Console.Write(pressage.KeyChar);
                }              
            }
        }

        public void PerformAction(ConsoleKeyInfo keyinfo)
        {
            if (Console.CursorLeft != 0)
            {
                Console.WriteLine();
            }

            Action action = Combinations[new Combination(keyinfo.Modifiers, keyinfo.KeyChar)];

            Thread thread;

            if ( Workers.Any(t => t.Name == action.Method.Name && t.IsAlive ))
            {
                thread = Workers.Where(t => t.Name == action.Method.Name).First();

                if ( thread.IsAlive)
                {
                    return;
                }
            }
        
            thread = new Thread(new ThreadStart(action))
            {
                Name = action.Method.Name,
                IsBackground = true,
                Priority = ThreadPriority.Lowest
            };
            Workers.Add(thread);

            thread.Start();

        }        
       
    }

}
