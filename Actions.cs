﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BusyDotBat
{
    public class Actions
    {
        internal static void WriteSomeStuff()
        {
            Console.WriteLine("Write Some Stuff");
        }

        internal static void Help()
        {
            
        }

        internal static void WriteSomeNumbers()
        {
            int start = 0;
            int stop = 1000000;

            for (int i = start; i<stop; i++)
            {
                Console.WriteLine(i);
                Thread.Sleep(100);
            }
        }

        internal static void WriteSomeStrings()
        {
            int start = 0;
            int stop = 1000000;

            int length = 7;
            Random random = new Random();

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            for (int i = start; i < stop; i++)
            {
               var temp = new string(Enumerable.Repeat(chars, length)
                    .Select(s => s[random.Next(s.Length)]).ToArray());
                Console.WriteLine(temp);
                Thread.Sleep(100);
            }

            
           
        }

        internal static void ListDirectories()
        {
            Loader.Load();
        }
    }
}
